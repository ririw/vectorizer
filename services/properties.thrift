namespace rs textvec.properties

typedef string content_id
typedef string property_id

exception NotFoundException { }

service Properties {
    content_id get_content_id(
        1: string url,
        2: property_id propertyid,
        3: i64 refresh_freq
    ),
    list<content_id> get_content_ids(
        1: property_id propertyid
    ),
    string get_url(
        1: content_id contentid,
        2: property_id propertyid
    ) throws (1: NotFoundException contentid_not_found),
}
