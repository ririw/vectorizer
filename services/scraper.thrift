namespace js scraper

typedef string content_id
typedef string property_id

exception NotFoundException { }

service Scraper {
    oneway void queue_url(
        1: string url,
        2: property_id propertyid,
        3: content_id contentid
    ),
    string get_content(
        1: content_id contentid,
        2: property_id propertyid
    ) throws (1: NotFoundException contentid_not_found),
}
