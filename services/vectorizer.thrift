namespace py vectorizer.serv

typedef string TaskId

enum VectorizationStatus {
    PENDING = 1,
    READY = 2,
    UNKNOWN = 3,
}

struct Vectorization {
    1: VectorizationStatus status,
    2: TaskId taskid,
    3: optional list<double> vector
}

service Vectorizer {
    Vectorization queue( 1: string content ),
    Vectorization retrieve ( 1: TaskId taskid ),
    Vectorization retrieve_now ( 1: TaskId taskid ),
}