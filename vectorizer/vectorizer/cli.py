import datetime
import logging
import uuid
from concurrent import futures

import coloredlogs
import diskcache as dc
from plumbum import cli
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
from thrift.transport import TSocket, TTransport

from vectorizer.serv import Vectorizer, ttypes

logger = logging.getLogger("server")


class VectorizerHandler:
    MAX_AGE = datetime.timedelta(days=1)

    def __init__(self):
        self.tasks = {}
        self.executor = None
        self._get_models()
        self.cache = dc.Cache()

    def _get_models(self):
        import transformers

        self.tokenizer = transformers.OpenAIGPTTokenizer.from_pretrained("openai-gpt")
        self.model = transformers.OpenAIGPTModel.from_pretrained("openai-gpt")

    def __enter__(self):
        self.executor = futures.ThreadPoolExecutor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.executor.shutdown()

    def queue(self, content):
        logger.info("Got -- {}".format(content))
        task_id = str(uuid.uuid4())
        task_fut = self.executor.submit(self.vectorize, content)
        now = datetime.datetime.now()
        self.tasks[task_id] = (task_fut, now)

        for tid, (fut, birth) in self.tasks.items():
            age = now - birth
            if age > self.MAX_AGE:
                fut.result()
                del self.tasks[tid]

        result = ttypes.Vectorization(
            status=ttypes.VectorizationStatus.PENDING,
            taskid=task_id,
            vector=None)
        logger.info("returned -- {}".format(result))
        return result

    def retrieve(self, task_id):
        return self._retrieve_inner(task_id, block=False)

    def retrieve_now(self, task_id):
        return self._retrieve_inner(task_id, block=True)

    def _retrieve_inner(self, task_id, block=False):
        if task_id == 'DEBUG':
            try:
                task_id = next(iter(self.tasks))
            except StopIteration:
                # Fall through to next part, which will return UNKNOWN
                pass

        if task_id not in self.tasks:
            return ttypes.Vectorization(
                status=ttypes.VectorizationStatus.UNKNOWN, taskid=task_id, vector=None
            )

        fut, _ = self.tasks[task_id]
        # If we aren't blocking, and the future isn't done,
        # we return pending.  If we are blocking, or if the
        # future is done, we move on to get the result.
        if not block and not fut.done():
            return ttypes.Vectorization(
                status=ttypes.VectorizationStatus.PENDING,
                taskid=task_id,
                vector=None,
            )

        result = fut.result()
        del self.tasks[task_id]
        return ttypes.Vectorization(
            status=ttypes.VectorizationStatus.READY, taskid=task_id, vector=result
        )

    def vectorize(self, content):
        if content in self.cache:
            return self.cache[content]

        import torch
        input_ids = torch.tensor(
            [self.tokenizer.encode(content, add_special_tokens=True)]
        )
        with torch.no_grad():
            last_hidden_states = self.model(input_ids)[0].mean(0).mean(0)
        res = [float(v) for v in last_hidden_states.detach().numpy()]
        self.cache[content] = res
        return res


class ServerInterface(cli.Application):
    def main(self, host="127.0.0.1", port=9090):
        coloredlogs.install()
        with VectorizerHandler() as handler:
            processor = Vectorizer.Processor(handler)
            transport = TSocket.TServerSocket(host=host, port=port)
            tfactory = TTransport.TBufferedTransportFactory()
            pfactory = TBinaryProtocol.TBinaryProtocolFactory()
            server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
            logger.info("Starting server")
            server.serve()


if __name__ == "__main__":
    ServerInterface.run()
