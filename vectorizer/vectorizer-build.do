redo-ifchange ../services/vectorizer.thrift vectorizer/** poetry.lock pyproject.toml
thrift -gen py  ../services/vectorizer.thrift
rm -rf vectorizer/serv
mv gen-py/vectorizer/serv vectorizer
rm -rf gen-py
poetry export -f requirements.txt > docker_requirements.txt
poetry build -f wheel
docker build -t vectorizer .
echo "DONE"
