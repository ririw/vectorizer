mod properties;

use postgres::{Client, Error as PSErr, NoTls, Row};
extern crate thrift;
use blake3;
use std::env;

fn setup() {
    let mut client = connect();
    client
        .execute(
            "CREATE TABLE IF NOT EXISTS property (property_id UUID PRIMARY KEY);",
            &[],
        )
        .unwrap();
    client
        .execute(
            "CREATE TABLE IF NOT EXISTS url (\
            rowid BIGSERIAL PRIMARY KEY,
            property_id TEXT NOT NULL, \
            url TEXT NOT NULL,\
            content_id TEXT UNIQUE NOT NULL);",
            &[],
        )
        .unwrap();
}

fn connect() -> Client {
    let host = env::var("TEXTVEC_POSTGRES_HOST").unwrap();
    let user = env::var("TEXTVEC_POSTGRES_USER").unwrap();
    let pass = env::var("TEXTVEC_POSTGRES_PASS").unwrap();

    let conn_str = format!("host={} user={} password={}", host, user, pass);
    let client = Client::connect(&conn_str, NoTls).unwrap();
    return client
}

pub struct PropertiesStruct {}

fn hash_url(url: &str, propertyid: &str) -> String {
    let url_text = url.as_bytes();
    let property_text = propertyid.as_bytes();
    let key = b"9CAAC885-FB77-43E0-9F7B-F6B363F715F3";

    let mut hasher = blake3::Hasher::new();
    hasher.update(&url_text);
    hasher.update(key);
    hasher.update(&property_text);
    let hash = hasher.finalize();
    return String::from(hash.to_hex().as_str());
}

fn map_to_app_error(e: PSErr) -> thrift::ApplicationError {
    return thrift::ApplicationError {
        kind: thrift::ApplicationErrorKind::InternalError,
        message: e.to_string(),
    };
}

impl properties::PropertiesSyncHandler for PropertiesStruct {
    fn handle_get_content_id(
        &self,
        url: String,
        propertyid: String,
        _refresh_freq: i64,
    ) -> Result<String, thrift::Error> {
        let mut client = connect();
        let rows = client
            .query(
                "\
            SELECT content_id
              FROM url
             WHERE url=$1
                   and property_id=$2
        ",
                &[&url, &propertyid],
            )
            .map_err(map_to_app_error)?;
        for row in rows {
            let cid: &str = row.get(0);
            return Ok(String::from(cid));
        }
        let cid: String = hash_url(url.as_str(), propertyid.as_str());

        client
            .execute(
                "\
            INSERT INTO url (property_id, url, content_id) VALUES ($1, $2, $3)
        ",
                &[&propertyid, &url, &cid],
            )
            .map_err(map_to_app_error)?;

        return Ok(cid);
    }

    fn handle_get_content_ids(&self, propertyid: String) -> Result<Vec<String>, thrift::Error> {
        fn get_row_entry(row: Row) -> String {
            let x: &str = row.get(0);
            return String::from(x);
        }
        let mut client = connect();
        let rows = client
            .query(
                "SELECT content_id FROM url WHERE property_id=$1",
                &[&propertyid],
            )
            .map_err(map_to_app_error)?;

        let mut res_vec = Vec::new();
        for row in rows {
            res_vec.push(get_row_entry(row))
        }

        return Ok(res_vec);
    }

    fn handle_get_url(
        &self,
        contentid: String,
        propertyid: String,
    ) -> Result<String, thrift::Error> {
        let mut client = connect();
        let rows = client
            .query(
                "\
            SELECT url
              FROM url
             WHERE content_id=$1
                   AND property_id=$2
        ",
                &[&contentid, &propertyid],
            )
            .map_err(map_to_app_error)?;
        for row in rows {
            let url_str: &str = row.get(0);
            return Ok(String::from(url_str));
        }
        return Err(thrift::Error::from(properties::NotFoundException {}));
    }
}

fn main() {
    setup();

    let listen_address = "0.0.0.0:9090";

    let i_tran_fact = thrift::transport::TFramedReadTransportFactory::new();
    let i_prot_fact = thrift::protocol::TCompactInputProtocolFactory::new();

    let o_tran_fact = thrift::transport::TFramedWriteTransportFactory::new();
    let o_prot_fact = thrift::protocol::TCompactOutputProtocolFactory::new();
    let processor = properties::PropertiesSyncProcessor::new(PropertiesStruct {});

    let mut server = thrift::server::TServer::new(
        i_tran_fact,
        i_prot_fact,
        o_tran_fact,
        o_prot_fact,
        processor,
        10,
    );

    println!("Bringing up server...");

    match server.listen(&listen_address) {
        Err(e) => panic!(e),
        Ok(_) => return (),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::properties::PropertiesSyncHandler;

    #[test]
    fn test_hash_stable() {
        let hash1 = hash_url("https://example.com", "p1");
        let hash2 = hash_url("https://example.com", "p1");
        let hash3 = hash_url("https://example.com", "p2");
        let hash4 = hash_url("https://example.com/foo", "p1");
        let hash5 = hash_url("https://example.com/foo?bar=1", "p1");
        assert_eq!(hash1, hash2);
        assert_ne!(hash1, hash3);
        assert_ne!(hash1, hash4);
        assert_ne!(hash4, hash5);
    }

    #[test]
    fn test_hash_transfer() {
        let hash1 = hash_url("https://example.com", "p1");
        let hash2 = hash_url("https://example.", "comp1");
        assert_ne!(hash1, hash2);
    }

    #[test]
    #[ignore]
    fn test_interface() {
        use uuid::Uuid;
        setup();
        let property = String::from("test-prop");
        let property_2 = String::from("test-prop-2");
        let mut client = connect();
        client.execute("DELETE FROM url WHERE property_id IN ($1, $2) ", &[&property, &property_2]).unwrap();
        let base = PropertiesStruct{};
        let uuid = Uuid::new_v4().to_hyphenated().to_string();
        let url = format!("{}{}", "example.com", uuid);
        let cid = base.handle_get_content_id(url.clone(), property.clone(), 0).unwrap();
        let cid_2 = base.handle_get_content_id(url.clone(), property.clone(), 0).unwrap();
        let expected_url = base.handle_get_url(cid.clone(), property.clone()).unwrap();

        assert_eq!(url, expected_url);
        assert_eq!(cid_2, cid);
        let found_cids = base.handle_get_content_ids(property.clone()).unwrap();
        assert!(found_cids.contains(&cid));
        assert_eq!(found_cids.len(), 1);
        let found_cids_p2 = base.handle_get_content_ids(property_2.clone()).unwrap();
        assert!(found_cids_p2.is_empty());
    }
}
