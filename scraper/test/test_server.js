const server = require('../bin/server')
const interfaces = require('../lib/interfaces')

const assert = require('assert')
const { describe, it } = require('mocha')

describe('the server', function () {
  it('should queue correctly', async function () {
    const serverImpl = server.serverRoutes

    await serverImpl.queue_url('http://example.com', '4321', 'test-property')
    const client = await interfaces.pool.connect()
    client.query('BEGIN')
    const results = await interfaces.processRow(client)
    client.query('COMMIT')
    assert.equal(results.url, 'http://example.com')
    assert.equal(results.contentid, '4321')
  })
})
