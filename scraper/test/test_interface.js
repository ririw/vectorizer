const interfaces = require('../lib/interfaces')
const assert = require('assert')
const { describe, it } = require('mocha')

async function deleteTestEntries () {
  const client = await interfaces.pool.connect()
  client.query("DELETE FROM scraping_task WHERE propertyid='test-property'")
  await client.query('COMMIT')
  assert.equal(1, 1)
}

describe('Interface tool', function () {
  it('should create tablestable creation', async function () {
    await interfaces.setup()
    assert.equal(1, 1)
  })

  it('should queue items', async function () {
    await interfaces.setup()
    await deleteTestEntries()
    await interfaces.enqueue('example.com', '1234', 'test-property')
    const client = await interfaces.pool.connect()
    client.query('BEGIN')
    const result = await interfaces.processRow(client)
    client.query('COMMIT')
    assert.equal(result.url, 'example.com')
    assert.equal(result.contentid, '1234')
  })

  it('should respond correctly with no entry', async function () {
    await interfaces.setup()
    await deleteTestEntries()
    const client = await interfaces.pool.connect()
    client.query('BEGIN')
    const result = await interfaces.processRow(client)
    client.query('COMMIT')
    assert.equal(result, null)
  })

  it('should multiqueue', async function () {
    await interfaces.setup()
    await interfaces.enqueue('example.com', '1234', 'test-property')
    await interfaces.enqueue('example.org', '1234', 'test-property')
    const client = await interfaces.pool.connect()
    client.query('BEGIN')
    const results1 = await interfaces.processRow(client)
    client.query('COMMIT')
    client.query('BEGIN')
    const results2 = await interfaces.processRow(client)
    client.query('COMMIT')
    assert.equal(results1.url, 'example.com')
    assert.equal(results2.url, 'example.org')
  })

  it('should save content', async function () {
    await interfaces.setup()
    await interfaces.saveContent('1234', 'test-property', 'hello world')
    const res = await interfaces.getContent('1234', 'test-property')
    assert.equal(res, 'hello world')
  })
})
