const thrift = require('thrift')
const Scraper = require('../lib/Scraper')
const scraperTypes = require('../lib/scraper_types')
const interfaces = require('../lib/interfaces')

const serverRoutes = {
  queue_url: async function (url, contentid, propertyid) {
    console.log('Queuing content: ', url, contentid, propertyid)
    await interfaces.enqueue(url, contentid, propertyid)
  },
  get_content: async function (contentId, propertyId) {
    const foundContent = await interfaces.getContent(contentId, propertyId)
    if (foundContent === null) {
      throw new scraperTypes.NotFoundException()
    } else {
      return foundContent
    }
  }
}

async function main () {
  // noinspection JSCheckFunctionSignatures
  const server = thrift.createServer(Scraper, serverRoutes)
  console.log('Starting server on port 9090')
  server.listen(9090)
}

module.exports = {
  serverRoutes: serverRoutes,
  main: main
}

main()
