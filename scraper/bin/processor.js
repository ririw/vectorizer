const interfaces = require('../lib/interfaces')
const scraping = require('../lib/scraping')

async function processQueue () {
  const client = await interfaces.pool.connect()
  await client.query('BEGIN TRANSACTION')
  let task = await interfaces.processRow(client)
  while (task != null) {
    console.log('Processing task ', task)
    const content = await scraping.processSite(task.url)
    await interfaces.saveContent(task.contentid, task.propertyid, content)
    task = await interfaces.processRow(client)
  }
  await client.query('COMMIT')
}

interfaces.setup().then(() => {
  setInterval(processQueue, 1000)
})
