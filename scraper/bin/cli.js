const program = require('commander')
const { processSite } = require('../lib/scraping')

function main () {
  var site = null
  program
    .arguments('<site>')
    .action(function (s) {
      site = s
    })
  program.parse(process.argv)

  console.log(processSite(site))
}

main()
