const request = require('request')
const util = require('util')
const htmlToText = require('html-to-text')

function extractHtml (response) {
  return response.body
}

function extractArticle (body) {
  return htmlToText.fromString(body, { wordwrap: 130 })
}

async function processSite (site) {
  return util.promisify(request.get)(site)
    .then(extractHtml)
    .then(extractArticle)
}

module.exports = {
  processSite: processSite
}
