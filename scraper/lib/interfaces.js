const { Pool } = require('pg')
// pools will use environment variables
// for connection information
const pool = new Pool()

async function setup () {
  await pool.query('CREATE TABLE IF NOT EXISTS scraping_task (' +
        ' id BIGSERIAL PRIMARY KEY,' +
        ' url TEXT,' +
        ' contentid TEXT,' +
        ' propertyid TEXT' +
        ' )')
  await pool.query('CREATE TABLE IF NOT EXISTS scraping_result (' +
      ' id BIGSERIAL PRIMARY KEY,' +
      ' contentid TEXT,' +
      ' propertyid TEXT,' +
      ' content TEXT)')
  return 'Ok'
}

async function saveContent (contentid, propertyid, content) {
  return pool.query('INSERT INTO scraping_result ' +
      ' (contentid, propertyid, content) ' +
      ' VALUES ($1, $2, $3)',
  [contentid, propertyid, content]
  )
}

async function enqueue (url, contentid, propertyid) {
  return pool.query('INSERT INTO scraping_task ' +
        ' (url, contentid, propertyid) ' +
        ' VALUES ($1, $2, $3)',
  [url, contentid, propertyid]
  )
}

async function processRow (client) {
  const queryResult = client.query('DELETE FROM scraping_task WHERE id=(' +
        ' SELECT id FROM scraping_task ORDER BY id FOR UPDATE SKIP LOCKED LIMIT 1' +
        ' ) RETURNING id, url, contentid, propertyid')
  return queryResult.then((rows) => {
    if (rows.rowCount === 0) {
      return null
    } else {
      return rows.rows[0]
    }
  })
}

async function getContent (contentId, propertyId) {
  const rows = await pool.query('SELECT content FROM scraping_result' +
      ' WHERE contentid=$1 ' +
      ' AND propertyid=$2',
  [contentId, propertyId])
  if (rows.rowCount === 0) {
    return null
  } else {
    return rows.rows[0].content
  }
}

module.exports = {
  pool: pool,
  setup: setup,
  enqueue: enqueue,
  processRow: processRow,
  saveContent: saveContent,
  getContent: getContent
}
