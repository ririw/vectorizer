redo-ifchange ../services/properties.thrift bin/** lib/** test/** package.json yarn.lock
thrift -gen js:node -out lib/ ../services/properties.thrift
thrift -gen js:node -out lib/ ../services/scraper.thrift
yarn install
yarn lint
docker build -t scraper .
echo "SCRAPER DONE"