use tempfile;
use std::io::{self, Write};
use std::fs::File;

pub struct Context<'a> {
    pub content_id:  &'a str,
    pub property_id: &'a str,
    pub content_vector: Vec<f32>,
    pub clicked: bool,
    pub shown: bool,
    pub prob: f32,
}

pub struct SelectFeature<'a> {
    pub options: Vec<Context<'a>>,
    pub current: Vec<f32>,
}


fn print_ctx_vec(vector: &Vec<f32>) -> String {
    let mut ix = 0;
    let mut str_res = "".to_owned();
    for v in vector.iter() {
        str_res.push_str(&format!("v{}={} ", ix, v));
        ix += 1
    }

    return str_res;
}

pub fn make_train_struct(instance: &SelectFeature) -> String {
    let mut shared_str = String::from("shared |").to_owned();
    shared_str.push_str(&print_ctx_vec(&instance.current));
    shared_str.push_str("\n");

    for opt in &instance.options {
        let ctx_str: &str = &print_ctx_vec(&opt.content_vector);
        let click_v = if opt.clicked {0} else {1};
        let newstr = if opt.shown {
            format!("0:{}:{} |", click_v, opt.prob)
        } else {
            String::from("|")
        };

        shared_str.push_str(&newstr);
        shared_str.push_str(ctx_str);
        shared_str.push_str("\n");
    }

    return shared_str
}

type TrainingData<'a> = Vec<SelectFeature<'a>>;

fn train_model(data: TrainingData, mut vw_model: File) -> io::Result<()> {
    let mut file: File = tempfile::tempfile()?;

    for d in data {
        writeln!(file, "{}", make_train_struct(&d))?;
    }

    writeln!(vw_model, "OK DONE")
}
