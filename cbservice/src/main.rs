use cbservice;

fn main() {
    let s1 = cbservice::SelectFeature {
        options: vec![
            cbservice::Context {
                content_id: "1",
                property_id: "1",
                content_vector: vec![0.0, 0.1, 0.2],
                clicked: false,
                shown: true,
                prob: 0.5,
            },
            cbservice::Context {
                content_id: "1",
                property_id: "1",
                content_vector: vec![0.1, 0.0, 0.2],
                clicked: false,
                shown: false,
                prob: 0.5,
            }

        ],
        current: vec![0.0, 1.3, 0.2],
    };
    println!("{}", cbservice::make_train_struct(&s1));
}
